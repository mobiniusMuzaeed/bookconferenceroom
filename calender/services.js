app.service('CalendarService', function($http) {
  this.signIn = function(data) {
    var promise = $http.post('/signin', data)
      .success(function(data, status) {
        //console.log?(data)
      })
      .error(function(data, status) {
        //console.log?(data)
      });

    return promise;
  }
  this.register = function(data) {
    var promise = $http.post('/register', data)
      .success(function(data, status) {
        //console.log?(data)
      })
      .error(function(data, status) {
        //console.log?(data)
      });

    return promise;
  }
  this.logout = function() {
    var promise = $http.post('/logout')
      .success(function(status) {
        //console.log?(data)
      })
      .error(function(status) {
        //console.log?(data)
      });
    return promise;
  }
  this.addEvent = function(data) {

    var returnEvent = $http.post('/addEvent', data)
      .success(function(data, status) {
        console.log(data)
      })
      .error(function(data, status) {
        //console.log?(data)
      });

    return returnEvent;
  }
  this.deletEvent = function(data) {
    //console.log?(data)
    var returnEvents = $http({
        url: '/deletEvent/' + data,
        method: "GET"
      }).success(function(data, status) {
        //console.log?(data)
      })
      .error(function(data, status) {
        //console.log?(data)
      });

    return returnEvents;
  }
  this.getAllEvents = function(data) {
    //console.log?('get all events')
    var returnAllEvents = $http({
        url: '/allEvents',
        method: "GET",
        params: {
          date: data
        }
      }).success(function(data, status) {
        //console.log?(data)
      })
      .error(function(data, status) {
        //console.log?(data)
      });


    return returnAllEvents;
  }
  this.editEvent = function(data) {
      var returnEvent = $http.put('/editEvent', data)
        .success(function(data, status) {
          //console.log?(data)
        })
        .error(function(data, status) {
          //console.log?(data)
        });

      return returnEvent;
    }
    //Send mail to participants

  this.sendMail = function(data) {
      console.log("-------------------");
      console.log(data);
      var returnEvent = $http.post('/sendMail', data)
        .success(function(data, status) {
          console.log(data)
        })
        .error(function(data, status) {
          //console.log?(data)
        });

      return returnEvent;
    }
    //checkevt service is used to check whether we can add event on that particular selected date and it
    //will return the result accordings to the date
  this.checkevt = function(rows, row, index) {
    temp = "event";
    temp_1 = "not a event"
    if (row[index] == " ")
      return temp_1; // when user select the empty date box
    else if (index == 0 || index == 6)
      return "holiday"; //when user select the holiday
    else
      return temp;
  }


  //month service is used to fetch the calendar month and will give the month details..
  this.month = function(rows, date) {
    date = new Date(date);
    curr_mon = date.getMonth();
    firstdate = new Date(date.getFullYear(), date.getMonth(), 1);
    lastdate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    fday = firstdate.getDate();
    lday = lastdate.getDate();
    startdate_index = firstdate.getDay();
    enddate_index = lastdate.getDay();
    temp = fday;
    monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    month_name = monthNames[curr_mon];
    year = date.getFullYear();
    key = month_name + " " + year;
    factory = rows;
    for (var i = 0; i < factory.length; i++) {
      for (var j = 0; j < 7; j++) {
        factory[i].value[j] = " ";
        // factory[i].event_name[j] = [];
        // factory[i].event_from_time[j] = [];
        // factory[i].event_to_time[j] = [];
      }
    }
    for (var i = 0; i < 6; i++) {
      for (var j = 0; j < 7; j++) {
        if (temp > lday) {
          break;
        } else {
          if (i == 0) {
            if (j >= startdate_index) {
              factory[i].value[j] = temp;
              //factory[i].clickable[j]=true;
              temp++;
            } else {
              factory[i].value[j] = " ";
            }
          } else {
            factory[i].value[j] = temp;
            //factory[i].clickable[j]=true;
            temp++;
          }
        }
      }
    }
    return factory;
  }
});
