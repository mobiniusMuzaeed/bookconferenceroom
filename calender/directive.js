app.directive('alert', function() {
  // body...
  var directive = {};
  directive.restrict = 'E';
  directive.template = "<div class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'><div class='modal-dialog' role='dailog'><div class='modal-content'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'></span></button><h4 class='modal-title ' id='myModalLabel'>Alert!!</h4></div><div class='modal-body' id='alert_body'>{{alert_ms}}</div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal' ng-click='close_modal1()'>Close</button><!-- <button type='button' class='btn btn-primary'>Save changes</button> --></div></div></div></div>";
  return directive;
});

app.directive('confirm', function() {
  // body...
  var directive = {};
  directive.restrict = 'E';
  // here we need to give the info. to users that which event they are going to delete?
  directive.template = "<div class='modal fade' id='myModal2' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'><div class='modal-dialog' role='dailog'><div class='modal-content'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'></span></button><h4 class='modal-title' id='myModalLabel'>Confirm</h4></div><div class='modal-body' id='alert_body'>Are you sure you want to Delete the {{ x.conferenceRoomNo }} event booked from {{ x.from | date:'hh:mm a'}} to {{ x.to | date:'hh:mm a'}}?</div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>No</button><button id='cancel_button' type='button' class='btn btn-primary' data-target='#myModal1' data-dismiss='modal' ng-click='confirmDeletEvent()'>Yes</button></div></div></div></div>";
  return directive;
});

app.directive('loginbox', function() {
  // body...
  var directive = {};
  directive.restrict = 'E';
  directive.template = "<div class='modal fade' id='myModal3' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'><div class='modal-dialog' role='dailog'><div class='modal-content'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button><h4 class='modal-title' id='myModalLabel'>LOGIN</h4></div><div class='modal-body' id='alert_body'>Please Login to perform any update!!</div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal' ng-click='close_login()'>No</button><button id='login_button' type='button' class='btn btn-primary' data-target='#myModal1' data-dismiss='modal' ng-click='open_login()'>Login</button></div></div></div></div>";
  return directive;
});

app.directive('login', function() {
  // body...
  var directive = {};
  directive.restrict = 'E';

  directive.templateUrl = "login/login.html";
  directive.controller = "logincntrl";
  return directive;
});
app.directive('eventList', function() {
  //loads the eventlist
  var directive = {};
  directive.restrict = 'E';
  directive.templateUrl = "listEvents/listEvent.html";
  return directive;
});
app.directive('numbersOnly', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, modelCtrl) {

      modelCtrl.$parsers.push(function(inputValue) {
        if (inputValue == undefined) return ''
        var transformedInput = inputValue.replace(/[^0-9]/g, '');
        if (transformedInput != inputValue || parseInt(transformedInput, 10) > parseInt(attrs.ngMax, 10)) {
          if (transformedInput != inputValue) {
            modelCtrl.$setValidity('nonnumeric', true);
          } else {
            modelCtrl.$setValidity('nonnumeric', true);
          }

          if (parseInt(transformedInput, 10) > parseInt(attrs.ngMax, 10)) {
            transformedInput = '';
            modelCtrl.$setValidity('abovemaximum', false);
          } else {

            modelCtrl.$setValidity('abovemaximum', true);
          }


          modelCtrl.$setViewValue(transformedInput);
          modelCtrl.$render();
          return transformedInput;
        }
        modelCtrl.$setValidity('nonnumeric', true);

        modelCtrl.$setValidity('abovemaximum', true);

        return transformedInput;
      });
    }
  };
});
