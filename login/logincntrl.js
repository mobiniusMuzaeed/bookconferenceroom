angular.module('loginModule', [])

.controller('logincntrl', function($scope, CalendarService) {
  $scope.type = 'login';
  $scope.label = 'Login';
  // $scope.isLoggined = false;

  $scope.login = function() {
      var data = {};
      data.email = $scope.user;
      data.password = $scope.password;
      CalendarService.signIn(data).then(function(response) {

        //console.log?(response.data);
        var userDetails = response.data;
        //console.log?(userDetails.id)
        $scope.login_alert_ms = "Sucessfully Logged In.";
        angular.element('#myModal1').modal('hide');
        $scope.User = userDetails.user.email.substr(0, userDetails.user.email.indexOf('@'));
        sessionStorage.setItem("userId", userDetails.user.id);
        sessionStorage.setItem("userName", $scope.User);
        // $scope.isLoggined = true;

      }, function(response) {
        $scope.login_alert_ms = '';
        //console.log?(response.data.error);
        $scope.login_alert_ms = response.data.error;
      })


      /*if($scope.user==sessionStorage.getItem("user")&&$scope.password==sessionStorage.getItem("password")){
      $scope.login_alert_ms = "Sucessfully Logged In.";
      angular.element('#myModal1').modal('hide');
        $scope.User=$scope.user.substr(0, $scope.user.indexOf('@'));
      }
      else{
$scope.login_alert_ms = "Invalid Username or Password.";
      }*/
    }
    $scope.logout = (function() {
    CalendarService.logout().then(function(response) {
      // var userDetails = response.data;
      window.location.reload();
      sessionStorage.setItem("userId", 0);
      sessionStorage.setItem("userName", 0);
      // $scope.isLoggined = false;
      document.getElementById("userDetilsDropdown").style.display = "none";
    })
  })
    // this is testing for registraton
  $scope.registerNewUser = function() {
    var data = {};
    data.firstName = $scope.firstName;
    data.lastName = $scope.lastName;
    data.email = $scope.email;
    data.password = $scope.password;
    data.mobile = $scope.mobile;
    data.dob = $scope.dob;
    data.gender = $scope.gender;

    // added on 5-jul-16
    CalendarService.register(data).then(function(response) {
      //console.log?(response.data);
      var userDetails = response.data;
      //console.log?(userDetails.id)
      $scope.login_alert_ms = "Registered successfully.";
      angular.element('#myModal1').modal('hide');
      // $scope.fullname = userDetails.user.firstName + " " + userDetails.user.lastName;
      $scope.User = userDetails.user.email.substr(0, userDetails.user.email.indexOf('@'));
      sessionStorage.setItem("userId", userDetails.user.id);
      sessionStorage.setItem("userName", $scope.User);
    }, function(response) {
      $scope.login_alert_ms = '';
      //console.log?(response.data.error);
      $scope.login_alert_ms = response.data.error;
    })


    /*if($scope.user==sessionStorage.getItem("user")&&$scope.password==sessionStorage.getItem("password")){
      $scope.login_alert_ms = "Sucessfully Logged In.";
      angular.element('#myModal1').modal('hide');
        $scope.User=$scope.user.substr(0, $scope.user.indexOf('@'));
      }
      else{
$scope.login_alert_ms = "Invalid Username or Password.";
      }*/
  }
  $scope.setRegister = function() {
    console.log("register");
    $scope.type = 'register';
    $scope.label = 'Register';
  }
  $scope.setLogin = function() {
    console.log("login");
    $scope.type = 'login';
    $scope.label = 'Login';
  }
  $scope.validate = function(email) {
    if (email != undefined) {
      var reg = /^[_a-zA-Z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
      var testvalidate = email.split('.');
      if (reg.test(email)) {
        var emailvalidate = testvalidate[testvalidate.length - 1];
        if (emailvalidate == 'com' || emailvalidate == 'in') {
          $scope.sucess = ''
          return true;
        } else {
          $scope.sucess = ''
          $scope.sucess = "please enter valid email user@mobinius.com";
          return false;
        }
      } else {
        $scope.sucess = ''
        $scope.sucess = "please enter valid email user@mobinius.com";
        return false;
      }
    }
  }
})
